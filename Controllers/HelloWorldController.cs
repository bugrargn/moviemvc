﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace MvcMovie.Controllers
{
    public class HelloWorldController : Controller
    {
        // 
        // GET: /HelloWorld/
        //public string Index()
        //{
        //    return "Merhaba Dünya";
        //}

        // Views/HelloWorld/Index.cshtml
        public IActionResult Index()
        {
            return View();
        }

        // Views/HelloWorld/Karsilama.cshtml
        // GET: HelloWorld/Karsilama?isim=abc&kacKez=4
        public IActionResult Karsilama(string isim, int kacKez = 1)
        {
            ViewData["Mesaj"] = "Merhaba " + isim;
            ViewData["KacKez"] = kacKez;
            return View();
        }

        // 
        // GET: /HelloWorld/Welcome/ 
        public string Welcome()
        {
            return "Welcome action method";
        }

        // GET: /HelloWorld/Hosgeldin?isim=abc&kacKez=4
        // Requires using System.Text.Encodings.Web;
        public string Hosgeldin(string isim, int kacKez = 1)
        {
            return HtmlEncoder.Default.Encode($"Merhaba {isim}, kacKez: {kacKez}");
        }

        // GET: /HelloWorld/Merhaba/3?isim=abc 
        // Requires using System.Text.Encodings.Web;
        public string Merhaba(string isim, int ID = 1)
        {
            return HtmlEncoder.Default.Encode($"Merhaba {isim}, ID: {ID} ");
        }

    }
}
