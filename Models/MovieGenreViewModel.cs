﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcMovie.Models
{
    // Film Türlerinide Aramak İçin.
    public class MovieGenreViewModel
    {
        public List<Movie> Movies { get; set; }
        // SelectList türlerin listesini içerir. Listeden Türü seçebilir.
        public SelectList Genres { get; set; }
        // Seçili Film Türü
        public string MovieGenre { get; set; }
        // Arama kutusuna girilen metin.
        public string SearchString { get; set; }
    }
}
